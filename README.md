# Goldbach's conjecture

Simple example to illustrate Goldbach's conjecture (not very efficient, but
enough to be a project for practicing with git repositories!).

- Crear un fork del repositorio de la conjetura de Golbach:
  https://gitlab.com/japostigo-atsistemas/goldbachs-conjecture.

- Clonar el nuevo repositorio (el fork) en local.

- Cambiar a la rama _develop_.

- Crear una rama _feature/refactorization_ (ese es el nombre de la rama,
  incluyendo la barra) que parta de _develop_, que contendrá las modificaciones
  oportunas para que la variable _potentialDivisor_ de la función _isPrime_ pase
  a llamarse simplemente _i_. No subas a remoto esta rama.

- Cuando la rama esté lista, cambia a _develop_ y _mergea_ en _develop_ el
  contenido de la rama _feature/refactorization_.

- _Pushea_ _develop_ al repositorio remoto.

- Un matemático del equipo reporta que para comprobar si un número _n_ es primo
  no hace falta comprobar sus divisores hasta _n – 1_, si no que basta con
  hacerlo hasta raíz cuadrada de _n_. Intenta comprender cual es el segmento de
  código en el que habría que hacer ese cambio. Crea una rama
  _hotfix/optimization_ que parta de _master_ que incluya esta modificación.
  No subas a remoto esta rama.

- Cuando la rama esté lista, _mergéala_ sobre _master_.

- Por último, _mergea_ en _máster_ lo que hay en _develop_ y _pushea_ _master_
  al repositorio remoto.
